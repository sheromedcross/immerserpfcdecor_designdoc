# Virtual Sentry System - ARK Mod
# Virtual Sentry System Mod
---
## Mod Design Document
---
## Overview
---  
### Questions

A virtual Sentry System mod for ARK that helps in protecting bases or other important structures.
* What is the mod?
  * A feature mod that adds a new form of area defense using temporary creatures to act as sentries in a localized area.

* Why create this mod?
  * To offer an alternative means of base defense for both online and offline protection that is not absolute but still substantial.

* What is the primary focus?
  * Players can sacrifice creatures they have tamed or bred to use as templates for sentry creatures.

* What are the win conditions?
  * Sentry creatures kill or fend off attacking enemies from a specific area.

* What are the lose conditions?
  * All sentry creatures are destroyed or the sentry tower is destroyed.

### Features
* The player can train up their own creatures to desired specifications to be sentries.
* Use of artifacts during the virtualization process can result in bonuses for the sentry.
* Artifacts can be "Unpacked" to find sentry tower mods to provide bonuses to tower functions.
* Sentry towers will spawn virtual versions of creatures that will attack anything designated as an enemy within range of the tower.
* If the sentry is destroyed it will be respawned after a designated period of time.
* If enemies leave the range of the tower the sentry will be dematerialized, this is also an automatic reset of its state and it can be instantly summoned again at any time if the enemies re-enter tower range.

---
## Details
---

### Overview

  Players will need to build a virtualization station to start. The station will have several pedestals around the outside that allow for the placement of artifacts. After placing a single creature in the station it can then be savrificed for virtualization. The creatures stats and information will be stored into a crystal item that can be attached to sentry towers. Sentry towers can house multiple crystals depending on how tall the tower is built. If the tower detects enemies within its range it will spawn all of the installed virtual sentries to attack. If all enemies in range are dead or have fled the area then the tower will despawn all the creatures. If all of the virtual sentries are defeated then the tower will shut down for a little while where it cannot respawn sentries. Artifacts can also be "Unpacked" to get augment crystals for the sentry towers. These can give bonuses like reduction in cooldown if creatures are destroyed.

### Structures

* Virtualization Station Base
* Virtualization Station Artifact Pedestals
* Sentry Tower Base
* Sentry Tower Segment (Sentry Module)
* Sentry Tower Segment (Augment Module)
* Sentry Crystals 
  * Variable
* Augment Crystals
  * Variable
  
### Items

* Virtualization Station Base
* Virtualization Station Artifact Pedestals
* Sentry Tower Base
* Sentry Tower Segment (Sentry Module)
* Sentry Tower Segment (Augment Module)
* Sentry Crystals
  * Variable
* Augment Crystals
  * Variable
